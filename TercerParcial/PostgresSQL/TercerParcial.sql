-- Table: public.Paises

-- DROP TABLE IF EXISTS public."Paises";

CREATE TABLE IF NOT EXISTS public."Paises"
(
    "Id" integer NOT NULL DEFAULT nextval('"Paises_Id_seq"'::regclass),
    "Continente" text[] COLLATE pg_catalog."default",
    "Código" numeric,
    "Pais-Territorio" text[] COLLATE pg_catalog."default",
    CONSTRAINT "Paises_pkey" PRIMARY KEY ("Id")
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public."Paises"
    OWNER to postgres;